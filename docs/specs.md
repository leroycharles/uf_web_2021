# Cahier des Charges

## Introduction

### Présentation du groupe

La convergence du paprika regroupe des étudiants du campus parisien du groupe Ynov afin de travailler sur des projets scolaires ou d’en monter de nouveaux afin de pouvoir s’améliorer tous ensemble et de manière cohérente.

### Nom du groupe

L’origine du nom de ce groupe change d’un projet à l’autre. Le nom seul n’a pas d’importance, seul les actions comptent.

### Logo

![Comment appelle-t-on un rongeur de l’espace ? Un hamsteroïde.](logo.jpg)

### Membres

#### Charles Leroy

Chef de projet, développeur et étudiant.

<!--PROJECT-->

## Spécificités Fonctionnelles
<!--user_stories-->

## Spécificités Techniques
<!--user_stories-->

## Conclusion

Ce projet semble contraignant à la vue de sa difficulté et des dead-lines imposées, il est nécessaire de prendre des précautions et de faire des retours réguliers afin d'éviter les rendus de dernières minutes.

- Le projet est disponible sur le repo suivant: [https://gitlab.com/leroycharles/uf_web_2021](https://gitlab.com/leroycharles/uf_web_2021).
- Le site du projet est disponible à l'adresse suivante: [https://leroycharles.gitlab.io/uf_web_2021](https://leroycharles.gitlab.io/uf_web_2021).
- Les releases du projet sont disponibles à l'adresse suivante: [https://gitlab.com/leroycharles/uf_web_2021/-/releases](https://gitlab.com/leroycharles/uf_web_2021/-/releases).
