# Implémenter une route de déconnexion

USER STORY: ref

## Descripion

Une route doit permettre la déconnexion de l'utilisateur

## Fonctions à implémenter

- [ ] Implémenter la route

## Tests à remplir

- [ ] Route::post->name("disconnect")

## Annexes

RAS
