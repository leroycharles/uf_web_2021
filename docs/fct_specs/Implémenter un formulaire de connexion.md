# Implémenter un formulaire de connexion

USER STORY: ref

## Descripion

Un formulaire doit pouvoir assurer la connexion d'un membre.

## Fonctions à implémenter

- [ ] Implémenter le formulaire
- [ ] Implémenter la route de conenxion

## Tests à remplir

- [ ] Route::post->name("connect")
- [ ] login.blade.php

## Annexes

RAS
