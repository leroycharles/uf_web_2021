# Définir un cahier des charges général, Définir une présentation du projet

USER STORY: Gestion de Projet

## Descripion

Définir un cahier des charges général en markdown pour permettre un développement rapide et une intégration continue performante. Définir une présentation du projet en LaTeX pour avoir un document PDF universel et efficace.

## Fonctions à implémenter

- [ ] Réaliser une description du projet dans le README.md
- [ ] Réaliser une description détaillé sans les user stories et les fct specs
- [ ] Réaliser la base d'une présentation BEAMER

## Tests à remplir

- [ ] Présence du fichier README.md
- [ ] Présence du fichier docs/specs.md
- [ ] Présence du fichier docs/presentation/main.tex

## Annexes

Le fichier LaTeX sera enrichie au cours des merge request