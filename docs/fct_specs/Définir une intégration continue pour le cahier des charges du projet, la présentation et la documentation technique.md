# Définir une intégration continue pour le cahier des charges du projet, la présentation et la documentation technique

USER STORY: Gestion de Projet

## Descripion

Définir une intégration continue pour le cahier des charges du projet avec mkDocs, la présentation avec LaTeX et la documentation technique avec Doxygen.

## Fonctions à implémenter

- [ ] Créer le script CI pour mkDocs et le CDC
- [ ] Créer le script CI pour LaTeX
- [ ] Créer le script CI pour Doxygen
- [ ] Créer le script CD pour Pages
- [ ] Créer les emplacements CI/CD pour les releases

## Tests à remplir

- [ ] Fichier .gitlab/scripts/specs_build.sh
- [ ] Fichier .gitlab/scripts/presentation_build.sh
- [ ] Fichier .gitlab/scripts/doxygen_build.sh
- [ ] Fichier .gitlab/.gitlab-ci.yml à jour pour les exports
- [ ] Fichier .gitlab/.gitlab-ci.yml à jour pour les releases

## Annexes

RAS
