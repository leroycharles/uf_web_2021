# Implémenter la vue de la gestion de billet

USER STORY: ref

## Descripion

Un chef de projet doit pouvoir gérer les billets depuis le tableau de gestion.

## Fonctions à implémenter

- [ ] Component ligne de billet
- [ ] Vue gestionnaire de billet

## Tests à remplir

- [ ] blog.manager.blade.php
- [ ] layout.component.blogline.blade.php

## Annexes

RAS
