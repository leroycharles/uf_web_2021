# Voir un billet

USER STORY: ref

## Descripion

Un billet doit pouvoir être vu au sein d'une page et de la timeline.

## Fonctions à implémenter

- [ ] Component billet
- [ ] Vue billet

## Tests à remplir

- [ ] billet.blade.php
- [ ] layout.component.billet.blade.php

## Annexes

RAS
