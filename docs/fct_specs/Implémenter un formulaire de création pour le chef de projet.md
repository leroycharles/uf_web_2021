# Implémenter un formulaire de création pour le chef de projet

USER STORY: ref

## Descripion

Le chef de projet doit pouvoir créer un membre à partir d'un formulaire

## Fonctions à implémenter

- [ ] Créer le formulaire dans Laravel
- [ ] Afficher une boîte de résumé à l'envoi
- [ ] Rediriger vers la gestion de l'équipe

## Tests à remplir

- [ ] profile.blade.php
- [ ] layout/form.profile.blade.php
- [ ] Affichage de la boîte
- [ ] Redirection permanente

## Annexes

RAS
