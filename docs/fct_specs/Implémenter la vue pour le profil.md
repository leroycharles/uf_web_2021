# Implémenter la vue pour le profil

USER STORY: ref

## Descripion

On utilise le layout pour fournir une vue du profil.

## Fonctions à implémenter

- [ ] Implémentation du layout Profile
- [ ] Gestion de la vue d'un profil

## Tests à remplir

- [ ] layout/profile.blade.php
- [ ] profile.blade.php

## Annexes
