# Implémenter la vue pour la page d'accueil

USER STORY: ref

## Descripion

Implémentation la vue pour la page d'accueil en récupérant la description de projet courte et les 3 derniers billets de la timeline.

## Fonctions à implémenter

- [ ] Récupérer la présentation courte du projet avec AJAX
- [ ] Récupérer les 3 derniers billets de blog avec le contrôleur associé
- [ ] Définir la vue

## Tests à remplir

- [ ] Fichier home.blade.php
- [ ] Fichier short.php
- [ ] Insertion de la description et des billets

## Annexes

RAS
