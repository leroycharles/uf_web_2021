# Implémenter un formulaire de création pour un membre

USER STORY: ref

## Descripion

Un membre doit pouvoir modifier un profil si c'est le sien.

## Fonctions à implémenter

- [ ] Créer le formulaire dans Laravel
- [ ] Afficher une boîte de résumé à l'envoi
- [ ] Afficher une boîte de confirmation pour la modification du mot de passe
- [ ] Rediriger vers le profil

## Tests à remplir

- [ ] profile.blade.php
- [ ] layout/form.profile.blade.php
- [ ] Affichage de la boîte de résumé
- [ ] Affichage de la boîte d'avertissement
- [ ] Redirection permanente

## Annexes

RAS
