# Définier une user storie par paquet minimum, Définir un diagramme UML pour les fonctionnalités du site

USER STORY: Gestion de Projet

## Descripion

Permet de définir les différentes userstories en mettant à jour la présentation LaTeX, le diagramme UML et le fichier de design.

## Fonctions à implémenter

- [ ] Cas du package VIRTINE
- [ ] Cas du package PLANNING
- [ ] Cas du package BLOG
- [ ] Cas du package COMMON

## Tests à remplir

- [ ] Fichiers .md pour les différentes UserStories des packages
- [ ] UseCases exportés pour les différentes UserStories des packages
- [ ] Frames ajoutés pour les différentes UserStories des packages dans le BEAMER

## Annexes

RAS