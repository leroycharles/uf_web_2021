# Implémenter un layout et ses components pour le site en mode membre visiteur

USER STORY: ref

## Descripion

Implémente un layout général avec une navbar et un footer. La navbar permet d'accéder au portail de connexion.

## Fonctions à implémenter

- [ ] layout/index.blade.php
- [ ] layout/components/navbar.php
- [ ] layout/components/footer.php

## Tests à remplir

- [ ] Fichier layout/index.blade.php
- [ ] Fichier layout/components/navbar.php
- [ ] Fichier layout/components/footer.php

## Annexes

RAS
