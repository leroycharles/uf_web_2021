# Implémenter un layout et ses components pour le site en mode membre connecté

USER STORY: ref

## Descripion

Implémente un layout général avec une navbar et un footer. La navbar permet d'accéder au profil si l'utilisateur est connecté.

## Fonctions à implémenter

- [ ] Layout général
- [ ] Accéder à la page de timeline
- [ ] Voir une liste des session en cours
- [ ] Accéder à une page de profile

## Tests à remplir

- [ ] Fichier layout/index.blade.php
- [ ] Fichier layout/components/navbar.php
- [ ] Fichier layout/components/footer.php

## Annexes

RAS
