# Implémenter la table des utilisateurs

USER STORY: ref

## Descripion

Le chef de projet doit pouvoir créer un membre à partir d'un formulaire

## Fonctions à implémenter

- [ ] Créer un tableau des utilisateurs
- [ ] Implémenter des bouttons de Delete et Read
- [ ] Implémenter des boutons de création et de profil

## Tests à remplir

- [ ] profile.blade.php
- [ ] layout/form.user.blade.php
- [ ] users.blade.php

## Annexes

RAS
