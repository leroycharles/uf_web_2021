# Un chef de projet doit pouvoir gérer des utilisateurs

## Descripion

Un chef de projet doit pouvoir gérer des utilisateurs avec un tableau.

## Spécification Technique Implémentées

Elle implémente la/les spécification(s) technique(s)suivante(s) :

- [ ] Implémenter un formulaire de création pour le chef de projet
- [ ] Implémenter la table des utilisateurs
- [ ] Implémenter un formulaire de modification pour un membre
- [ ] Implémenter la vue pour le profil

## Détails de la USER STORIE

Un chef de projet doit pouvoir gérer des utilisateurs avec un tableau. Il est le seul à pouvoir créer les mots de passe et définir des rôles.

## Annexes

RAS
