# Un chef de projet doit pouvoir traiter une demande de contact

## Descripion

Un visiteur peut envoyer une demande de contact via un formulaire au sein de la page CONTACT

## Spécification Technique Implémentées

Elle implémente la/les spécification(s) technique(s)suivante(s) :

- [ ] Un visiteur peut envoyer une demande de contact
- [ ] Un chef de projet doit pouvoir lire une demande de contact
- [ ] Un chef de projet doit pouvoir répondre une demande de contact
- [ ] Un chef de projet doit pouvoir envoyer une réponse automatique une demande de contact
- [ ] Un chef de projet doit pouvoir supprimer une demande de contact

## Détails de la USER STORIE

On implémente un CRD (et non un CRUD) pour les demandes de contact. Il s'uffit d'^tre visiteur pour faire une demande de contact. Il faut être chef de projet pour traiter une demande de contact.

## Annexes

Les demandes de contact incluent:

- un nom
- un prénom
- une adresse mail de réponse valide
- un motif via une liste de choix (brochure, flux RSS, newsletter, IGPD, autre)
- un message d'au moins 8 caractères et de max 500 caractères.
- une date et heure d'envoi
