# Un membre doit pouvoir modifier son profil

## Descripion

Un membre doit pouvoir accéeder à un profil et le modifier si c'est le sien.

## Spécification Technique Implémentées

Elle implémente la/les spécification(s) technique(s)suivante(s) :

- [ ] Implémenter un formulaire de création pour le chef de projet
- [ ] Implémenter un formulaire de modification pour un membre
- [ ] Implémenter la vue pour le profil

## Détails de la USER STORIE

La page Profile permet de modifier son profil ou d'afficher un profil demander.
Pour le chef de projet cela permet de créer un utilisateur.

## Annexes

RAS
