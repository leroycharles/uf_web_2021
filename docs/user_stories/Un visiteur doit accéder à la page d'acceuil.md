# Un visiteur doit accéder à la page d'acceuil

## Descripion

Un utilisateur doit pouvoir accéeder à la page d'accueil et au menu.

## Spécification Technique Implémentées

Elle implémente la/les spécification(s) technique(s)suivante(s) :

- [ ] Implémenter un layout et ses components pour le site en mode visiteur
- [ ] Implémenter un layout et ses components pour le site en mode membre connecté
- [ ] Implémenter la vue pour la page d'accueil

## Détails de la USER STORIE

La page d'accueil permet de voir une présentation courte du projet ainsi que les 3 derniers articles de la timeline.
En outre la merge request implémente le layout général du site en prenant en compte l'authentification.

## Annexes

RAS
