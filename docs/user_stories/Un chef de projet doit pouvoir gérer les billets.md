# Un chef de projet doit pouvoir gérer les billets

## Descripion

Un chef de porjet doit pouvoir gérer les billets.

## Spécification Technique Implémentées

Elle implémente la/les spécification(s) technique(s)suivante(s) :

- [ ] Modifier un billet
- [ ] Créer un billet
- [ ] Supprimer un billet
- [ ] Implémenter la vue de la gestion de billet
- [ ] Voir un billet

## Détails de la USER STORIE

Un chef de projet doit pouvoir gérer les billets à partir d'un tableau de gestion rapide.
Il doit également pouvoir créer un billet.

## Annexes

Le tableau de gestion propose de voir les 25 premiers caractères du contenu d'un billet.
