# Un visiteur doit voir tous les billets dans la timeline

## Descripion

Un visiteur doit voir tous les billets dans la timeline.

## Spécification Technique Implémentées

Elle implémente la/les spécification(s) technique(s)suivante(s) :

- [ ] Voir un billet
- [ ] Page de la Timeline

## Détails de la USER STORIE

La vue de la timeline reprend la vue d'un billet pour chaque billet.

## Annexes

RAS
