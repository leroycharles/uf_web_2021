# Gestion de Projet

## Description

Rendu initial permettant la publication du cahier des charges, le lancement de l'intégration continue, la définition du planning, etc

## Date de Release

Mercredi 27 janvier 23h42.

### Milestone

%"Gestion de Projet"

### Merge Request

Pas de sous merge request.

### Issues

- [ ] Définir un cahier des charges général
- [ ] Définir une présentation du projet
- [ ] Définier une user storie par paquet minimum
- [ ] Définir un diagramme UML pour les fonctionnalités du site
- [ ] Définir une intégration continue pour le cahier des charges du projet
- [ ] Définir une intégration continue pour la présentation du projet
- [ ] Définir une intégration continue pour la documentation du projet
- [ ] Réfléchir à des options de déploiements continues
