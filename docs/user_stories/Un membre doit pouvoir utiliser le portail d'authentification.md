# Un membre doit pouvoir utiliser le portail d'authentification

## Descripion

Un membre doit pouvoir se connecter. S'il est connecté il doit pouvoir se déconnecter.

## Spécification Technique Implémentées

Elle implémente la/les spécification(s) technique(s)suivante(s) :

- [ ] Implémenter un formulaire de connexion.
- [ ] Implémenter une route de déconnexion.

## Détails de la USER STORIE

Un membre doit pouvoir se connecter. S'il est connecté il doit pouvoir se déconnecter.

## Annexes

RAS
