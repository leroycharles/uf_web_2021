# Un visiteur doit avoir accès à des billets de blog dans la timeline

## Descripion

La Timeline représente la partie blog de la vitrine, chaque billet est associé à une date qui est un évènement important du projet. Les releases font l'objet d'un billet par exemple. L'ajout d'un membre important à l'équipe aussi.

## Spécification Technique Implémentées

Elle implémente la/les spécification(s) technique(s)suivante(s) :

- [ ] Afficher un billet de blog pour un utilisateur
- [ ] Afficher la timeline sous forme d'une liste
- [ ] Un chef de projet doit pouvoir ajouter un billet de blog
- [ ] Un chef de projet doit pouvoir modifier un billet de blog
- [ ] Un chef de projet doit pouvoir supprimer un billet de blog

## Détails de la USER STORIE

La Timeline représente la partie blog de la vitrine, chaque billet est associé à une date qui est un évènement important du projet. Les releases font l'objet d'un billet par exemple. L'ajout d'un membre important à l'équipe aussi. On effectue un CRUD pour les billets de blog où seul l'affichage d'un billet ne nécessite pas d'authentification.

## Annexes

Un billet de blog contient les informations suivantes:

- nom, prénom de l'auteur
- un titre
- un contenu
- une date
