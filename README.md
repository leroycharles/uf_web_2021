## Interface de Gestion de Projet Dynamique

### Présentation

#### Origine

Ce projet nous a été confier dans le cadre d'une évaluation de nos compétences des différentes technologies Web vues au cours de l'année.

#### Nature

Ce projet prend la forme d'un site internet, développé avec le framework Laravel (PHP), jQuery & jQueryUI (JavaScript) et Bootstrap(CSS).

#### But et intérêt

Ce projet propose une interface de gestion de projet, avec vitrine, proposant un contenu adapté au type d'utilisateur qui le parcours.

#### Position dans le secteur

Ce projet est un projet scolaire, il n’a pas vocation à être mis en compétition ou sur un marché.

### Fonctionnel

#### Rappel Contextuel

Ce projet propose une interface de gestion de projet, avec vitrine, proposant un contenu adapté au type d'utilisateur qui le parcours.

#### Fonctionnalités du jeu

Ce site possède 5 niveaux de vues:

- **le niveau administrateur:** il s'agit du développeur qui va créer le site à partir des informations fournis par le chef de projet;
- **le niveau manager:** le site présente toutes les informations qu'il possède au chef de projet;
- **le niveau membre:** le site présente les informations du projet, notamment l'organisation des spécificités techniques, les tâches en cours, etc, ainsi que toutes les informations qui suivent;
- **le niveau client:** le site présente les informations relatives à l'avancement du projet et un historique des livrables, ainsi que toutes les informations qui suivent;
- **le niveau visiteur:** le site propose une vitrine du projet à tout utilisateur non connecté.

### Aspect Techniques et Méthodologique

#### Moyens Matériels

Le développement s’effectuera avec la suite de logiciel JetBrains sous licence étudiante.
Le serveur git sera hébergé sur la plate-forme Gitlab.com avec les fonctionnalités gratuites, le GitFLow sera gérer par des plugins intégrés aux logiciels JetBrains et grâce à la version gratuite de GitKrakenGUI.

#### Moyens Intellectuels

Afin de pouvoir mener à bien ce projet, nous avons utilisé nos notes des cours suivis à Ynov ainsi que les notes des cours de Linkedin Learning "Maîtriser l'utilisation des librairies jQuery et jQuery UI" et "Maîtriser les frameworks PHP".

#### Aspects Économique

Ceci est un projet étudiant, il ne possède pas de financement, le budget est donc de 0€.
