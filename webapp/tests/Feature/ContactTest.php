<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ContactTest extends TestCase
{
    public function testIndex()
    {
        $response = $this->get('/contacts');
        $response->assertStatus(200);
    }

    public function testShow()
    {
        $this->assertDatabaseHas('contacts', ['id' => '1']);
        $response = $this->get('/contacts/1');
        $response->assertStatus(200);
    }

    public function testCreate()
    {
        $response = $this->get('/contacts/create');
        $response->assertStatus(200);
    }

    public function testEdit()
    {
        $this->assertDatabaseHas('contacts', ['id' => '1']);
        $response = $this->get('/contacts/1/edit');
        $response->assertStatus(200);
    }
}
