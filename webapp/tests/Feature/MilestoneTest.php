<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class MilestoneTest extends TestCase
{

    public function testIndex()
    {
        $response = $this->get('/milestones');
        $response->assertStatus(200);
    }

    public function testShow()
    {
        $this->assertDatabaseHas('milestones', ['id' => '1']);
        $response = $this->get('/milestones/1');
        $response->assertStatus(200);
    }

    public function testCreate()
    {
        $response = $this->get('/milestones/create');
        $response->assertStatus(200);
    }

    public function testEdit()
    {
        $this->assertDatabaseHas('milestones', ['id' => '1']);
        $response = $this->get('/milestones/1/edit');
        $response->assertStatus(200);
    }
}
