<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class SessionTest extends TestCase
{
    public function testIndex()
    {
        $response = $this->get('/sessions');
        $response->assertStatus(200);
    }

    public function testShow()
    {
        $this->assertDatabaseHas('sessions', ['id' => '1']);
        $response = $this->get('/sessions/1');
        $response->assertStatus(200);
    }

    public function testCreate()
    {
        $response = $this->get('/sessions/create');
        $response->assertStatus(200);
    }

    public function testEdit()
    {
        $this->assertDatabaseHas('sessions', ['id' => '1']);
        $response = $this->get('/sessions/1/edit');
        $response->assertStatus(200);
    }
}
