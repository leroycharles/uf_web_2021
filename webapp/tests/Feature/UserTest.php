<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use App\Http\Controllers\UsersController;
use Tests\TestCase;

class UserTest extends TestCase
{
    public function testIndex()
    {
        $response = $this->get('/users');
        $response->assertStatus(200);
    }

    public function testShow()
    {
        $this->assertDatabaseHas('users', ['id' => '1']);
        $response = $this->get('/users/1');
        $response->assertStatus(200);
    }

    public function testCreate()
    {
        $response = $this->get('/users/create');
        $response->assertStatus(200);
    }

    public function testEdit()
    {
        $this->assertDatabaseHas('users', ['id' => '1']);
        $response = $this->get('/users/1/edit');
        $response->assertStatus(200);
    }
}
