<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ReportTest extends TestCase
{
    public function testIndex()
    {
        $response = $this->get('/reports');
        $response->assertStatus(200);
    }

    public function testShow()
    {
        $this->assertDatabaseHas('reports', ['id' => '1']);
        $response = $this->get('/reports/1');
        $response->assertStatus(200);
    }

    public function testCreate()
    {
        $response = $this->get('/reports/create');
        $response->assertStatus(200);
    }

    public function testEdit()
    {
        $this->assertDatabaseHas('reports', ['id' => '1']);
        $response = $this->get('/reports/1/edit');
        $response->assertStatus(200);
    }
}
