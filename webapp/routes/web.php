<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


//Resources for all ELOQUENT ORM
Route::resource('/users', 'App\Http\Controllers\UsersController');
Route::resource('/reports', 'App\Http\Controllers\ReportController');
Route::resource('/sessions', 'App\Http\Controllers\SessionController');
Route::resource('/contacts', 'App\Http\Controllers\ContactController');
Route::resource('/milestones', 'App\Http\Controllers\MilestoneController');
